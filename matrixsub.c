#include <stdio.h>

#include <stdio.h>

int main()
{
	int a1,b1,a2,b2;
	printf("HOW MANY ROWS YOU WANT IN THE FIRST ARRAY: ");
	scanf("%d",&a1);
	printf("HOW MANY COLUMNS YOU WANT IN THE FIRST ARRAY: ");
	scanf("%d",&b1);
	int a[a1][b1];
	for (int i=0;i<a1;i++)
		for(int j=0;j<b1;j++)
		{
			printf("ENTER AN ELEMENT: ");
			scanf("%d",&a[i][j]);
		}
	printf("HOW MANY ROWS YOU WANT IN THE SECOND ARRAY: ");
	scanf("%d",&a2);
	printf("HOW MANY COLUMNS YOU WANT IN THE SECOND ARRAY: ");
	scanf("%d",&b2);
	int b[a2][b2];
	for (int i=0;i<a2;i++)
		for(int j=0;j<b2;j++)
		{
			printf("ENTER AN ELEMENT: ");
			scanf("%d",&b[i][j]);
		}
	if (a1==a2 && b1==b2)
	{
		int c[a1][b1];
		for (int i=0;i<a1;i++)
			for(int j=0;j<b1;j++)
			{
				c[i][j]=a[i][j]-b[i][j];
			}
		printf("THE FINAL MATRIX IS: ");
		for (int i=0;i<a1;i++)
		{
			printf("\n");
			for(int j=0;j<b1;j++)
			{
				printf("%d \t",c[i][j]);
			}
		}
	}
	else
		printf("MATRIX SUBTRACTION NOT POSSIBLE");
	return 0;
}
	